package controller;

import java.awt.Color;
import java.io.File;

import model.DrawingContainer;
import model.ReadAndWriteShapes;
import model.Rect;
import View.ViewShapes;

public class controler  {
	
	private ViewShapes vs;
	private ReadAndWriteShapes m;
	String pathname;
	public controler()
	{
		vs=new ViewShapes(this);
		vs.setUp(vs.newDC());
		m=new ReadAndWriteShapes();
		pathname="bilder.dat";
		readShape();
	}
	public void handleEvent(String ch)	
	{
		
		if(ch.equals("s"))
		{		
		m.saveShapes(vs.vectorShapes(), pathname);
		}
		if(ch.equals("r"))
		{
			readShape();			
		}	
		if(ch.equals("rec"))
		{			
			vs.setRecFlag(true);	
			vs.setCirFlag(false);
			vs.setLineFlag(false);	
			vs.setMoveFlag(false);
		}
		if(ch.equals("cir"))
		{
			vs.setCirFlag(true);
			vs.setRecFlag(false);
			vs.setLineFlag(false);	
			vs.setMoveFlag(false);
		}
		if(ch.equals("li"))
		{
			vs.setLineFlag(true);
			vs.setRecFlag(false);
			vs.setCirFlag(false);
			vs.setMoveFlag(false);
			
		}	
		if(ch.equals("cl"))
		{
			m.saveShapes(vs.vectorShapes(), pathname);
			m.exit();
		}
		if(ch.equals("cle"))
		{
			vs.repaint();
		}
		if(ch.equals("m"))
		{
			vs.setMoveFlag(true);
			vs.setLineFlag(false);
			vs.setRecFlag(false);
			vs.setCirFlag(false);			
		}
		if(ch.equals("del"))
		{
			vs.delete();
		}
		if(ch.equals("move"))
		{
			vs.move();
		}
		if(ch.equals("change"))
		{
			vs.changeSize();
		}
	} 
	public void readShape()
	{			
		vs.repaint();
		vs.readShapes(m.readShapes(pathname));
	}
	
}
