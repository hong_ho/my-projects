package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class SaveListener implements ActionListener{
	private ViewShapes vs;
	public SaveListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("s");
		
	}

}
