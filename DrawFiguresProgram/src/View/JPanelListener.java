package View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class JPanelListener implements MouseListener {

	private int originX, originY;
	@Override
	public void mouseClicked(MouseEvent e) {
		 originX = e.getX();
		  originY = e.getY();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if(x<originX||y<originY)
		{
			originX=x;
			originY=y;
		}
	}

}
