package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MoveListener implements ActionListener{
	private ViewShapes vs;
	public MoveListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("m");
		
	}

}
