import javax.swing.JButton;


public class Ruta extends JButton
{
	private boolean empty;
	private int rad, col;
	
	
	public Ruta(int rad, int col)
	{
		this.empty = true;
		this.rad = rad;
		this.col = col;
	}
	public boolean isEmpty()
	{
		return empty;
	}
	public void setEmpty(boolean empty) 
	{
		this.empty = empty;
	}	
	public int getRad() 
	{
		return rad;
	}
	public void setRad(int rad) 
	{
		this.rad = rad;
	}
	public int getCol()
	{
		return col;
	}
	public void setCol(int col) 
	{
		this.col = col;
	}
	public void setSymbol(int player)
	{		
		if (this.isEmpty())
		{
			if (player == 1)
				this.setText("X");
			else
				this.setText("O");
			this.setEmpty(false);
		}
	}	
}
