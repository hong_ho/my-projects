import java.awt.*;
import java.applet.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;
import java.util.Random;


public class Main extends Applet implements Runnable, MouseListener
{ // double buffering variable
	Image dbImage;
	Graphics dpGraphics;
	Thread th=new Thread(this);
	boolean going=true;
	
	int x,y,dx,dy,radius;
	Random r=new Random();
	
	public void init()
	{
		// kbao diem dau cho boll
		x=getSize().width/2; // w,h/2 neu ko thi nam o goc duoc mhinh va thay 1/4 boll
		y=getSize().height/2;
		dx=2;
		dy=2;
		radius=15;
		
		// cach dung chuot fai implements MouseListener
		addMouseListener(this);
	}
	
	public void mousePressed(MouseEvent e) 
	{
		Rectangle2D.Double mouse_bounds=new Rectangle2D.Double(e.getX()-1,e.getY()-1,2,2);
		Ellipse2D.Double ball_bounds=new Ellipse2D.Double(x-radius,y-radius,radius*2,radius*2);
		
		// boll dang nhay, neu minh banh vao boll thi no nhay di random huong
		if(ball_bounds.intersects(mouse_bounds))
		{
			x=r.nextInt(getSize().width);
			y=r.nextInt(getSize().height);
			// moi lan banh trung boll thi lam toc do boll chay nhanh hon
			if(dx<0)
				dx -=1;
			if(dx>0)
				dx +=1;
			if(dy<0)
				dy -=1;
			if(dy>0)
				dy +=1;
		}
		
	}
	public void mouseReleased(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	
	public void run()
	{
		while(going)
		{
			//boll move
			x += dx; // boll move- moi lan move 2
			y +=dy;
			
			// check tr�ffa 4 walls
			if(x<0)
				dx=Math.abs(dx);
			if(x> getSize().width)
				dx=-Math.abs(dx);
			if(y<0)
				dy=Math.abs(dy);
			if(y> getSize().height)
				dy=-Math.abs(dy);
			
			repaint();
			try
			{
				Thread.sleep(20);
			}
			catch(InterruptedException ie)
			{
				
			}
		}
	}
	public void paint(Graphics g)
	{
		// ve boll
		g.setColor(Color.blue);
		g.fillOval(x-radius, y-radius, radius*2, radius*2);
	}
	public void update(Graphics g)
	{
		if(dbImage==null)
		{
			dbImage=createImage(this.getSize().width, this.getSize().height);
			dpGraphics=dbImage.getGraphics();
		}
		dpGraphics.setColor(this.getBackground());
		dpGraphics.fillRect(0, 0, this.getSize().width, this.getSize().height);
		dpGraphics.setColor(this.getForeground());
		paint(dpGraphics);
		g.drawImage(dbImage,0,0,this);
		
	}
	public void start()
	{
		th.start();
	}
	public void stop()
	{
		going=false;
	}
	public void destroy()
	{
		going=false;
	}
	
}
