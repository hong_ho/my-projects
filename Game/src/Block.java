import java.awt.Color;
import java.awt.Graphics;


public class Block 
{
	public int x, y, width=64, height=16;
	public boolean alive=true;
	
	public Block(int x, int y)
	{
		this.x=x;
		this.y=y;		
	}
	public void draw(Graphics g)
	{
		g.setColor(Color.white);
		g.fillRect(x, y, width, height);
	}
}
