import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


public class GameTwoPlayer implements Moveable
{
	int positionX, positionY, x, y, width, height;
	public GameTwoPlayer(int x, int y, int w, int h)
	{
		width=w;
		height=h;		
		positionX=150;
		positionY=170;
	}
	public void move()
	{
		positionX +=x;
		positionY +=y;
		
	}
	public void collision(GameTwoObject go)
	{
		/*Rectangle r=new Rectangle(positionX+x,positionY+y,width,height);
		if(r.intersects(b.getBounds()))
		{
			x=0;
			y=0;
		}*/
		if(go instanceof GameTwoBlock)
		{
			x=0;
			y=0;
		}
		else if(go instanceof GameTwoGhost)
		{
			GameTwoState.HERO_LIFES--;
		}
	}	
	public Rectangle getBounds()
	{
		return new Rectangle(positionX+x, positionY+y, width, height);
	}
	public void draw(Graphics g)
	{
	g.setColor(Color.blue);
	g.fillRect(positionX, positionY, width, height);
	}
}
