import java.applet.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.*;
import java.util.ArrayList;
import java.util.Random;


public class MainBall extends Applet implements Runnable, KeyListener
{
 // double buffering variable
	Image dbImage;
	Graphics dpGraphics;
	
	Thread th=new Thread(this);
	private boolean running=true;
	
	private int fps=50;
	private Ball ball;
	private Paddle paddle;
	private ArrayList<Block> blocks=new ArrayList<Block>();
	//private Block block;
	private String level[]= {".........................", // 400/26=25 nen 25 block 1 rad va 25 kol
							 ".........................",
							 "...b.....................",
							 ".........................",
							 ".........................",
							 ".........................",
							 "........b................",
							 ".........................",
							 "...................b.....",
							 ".........................",
							 ".........................",
							 ".........................",
							 "....b....................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 ".........................",
							 "........................."};
	public void init()
	{
		ball=new Ball(getSize().width/2,1,10,Color.white);
		paddle=new Paddle(0, getSize().height-32);
	//	block=new Block(getSize().width/2,getSize().height/2);
		
		for(int y=0; y<level.length; y++)
		{
			for(int x=0; x<level[y].length();x++)
			{
			if(level[y].charAt(x)=='b')
				blocks.add(new Block(x*16,y*16));
			}
		}
		
		
		
		
		// cach dung chuot fai implements MouseListener		
		addKeyListener(this);
	}
	
	
	
	public void run()
	{
		while(running)
		{
			ball.move(getSize(), paddle);
			paddle.move(getSize().width);
			
			for(int i=0; i<blocks.size();i++)
			{
				ball.checkCollision(blocks.get(i));//ktra de remove
				if(!blocks.get(i).alive)
				{
					
					blocks.remove(i);
				}
			}
			repaint();
			try
			{
				Thread.sleep(20);
			}
			catch(Exception ie)
			{
				
			}
		}
	}
	public void paint(Graphics g)
	{
		g.fillRect(0, 0, getSize().width, getSize().height);// to man hinh thanh den
		ball.draw(g);// ve boll
		paddle.draw(g);
		//block.draw(g);
		for(Block b : blocks)
		{
			b.draw(g);
		}
	}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode()==KeyEvent.VK_LEFT)
			paddle.goingLeft=true;
		if(e.getKeyCode()==KeyEvent.VK_RIGHT)
			paddle.goingRight=true;
	}
	public void keyReleased(KeyEvent e)
	{
		if(e.getKeyCode()==KeyEvent.VK_LEFT)
			paddle.goingLeft=false;
		if(e.getKeyCode()==KeyEvent.VK_RIGHT)
			paddle.goingRight=false;
		
	}
	public void keyTyped(KeyEvent e)
	{
		
	}
	public void update(Graphics g)
	{
		if(dbImage==null)
		{
			dbImage=createImage(this.getSize().width, this.getSize().height);
			dpGraphics=dbImage.getGraphics();
		}
		dpGraphics.setColor(this.getBackground());
		dpGraphics.fillRect(0, 0, this.getSize().width, this.getSize().height);
		
		dpGraphics.setColor(this.getForeground());
		paint(dpGraphics);
	
		g.drawImage(dbImage,0,0,this);
		
	}
	public void start()
	{
		th.start();
	}
	public void stop()
	{
		running=false;
	}
	public void destroy()
	{
		running=false;
	}
	
}



