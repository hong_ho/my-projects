import java.awt.Rectangle;


public interface GameTwoObject 
{
	
	public Rectangle getBounds();
	
}
