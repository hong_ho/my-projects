
public interface Moveable extends GameTwoObject
{
	public void collision(GameTwoObject go);
}
