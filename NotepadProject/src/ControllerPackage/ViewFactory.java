package ControllerPackage;

import java.util.Scanner;

import ControllerPackage.NotepadController;
import View.ViewGui;
import View.ViewInterface;
import View.ViewConsole;


public class ViewFactory {
	private NotepadController c;
	
	public ViewFactory(NotepadController con)
	{
		this.c=con;
	}
	public ViewInterface createView()
	{
		
		Scanner in=new Scanner(System.in);
		
		System.out.println("Mata in 'c' f�r ViewConsole och 'g' f�r ViewGui");
		String s=in.nextLine();
		if(s.equals("g"))
			 return new ViewGui(c);
		if(s.equals("c"))
			return new ViewConsole(c);
		return null;
	}

}
