package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CutListener implements ActionListener{

	private ViewGui v;
	public CutListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(6);
		
	}

}