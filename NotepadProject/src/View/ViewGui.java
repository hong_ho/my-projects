package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.TextArea;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileReader;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

import ControllerPackage.NotepadController;
import View.TextChanged;

public class ViewGui extends JFrame implements ViewInterface
{
	private JTextArea textArea;
	private JMenuBar menuBar;
	private JMenu file,edit;
	private JMenuItem newFile,openFile,saveFile,saveAsFile,exit,copy,cut,paste,print,search,back;
	private JPanel panel;
	private JToolBar tools;
	
	private NotepadController c;
	private TextChanged tc;
	
	private String filename="Notepad";
	
	
	
	public ViewGui(NotepadController c)
	{		
		this.c=c;		
		addComponents();
		toolsSetUp();
	}
	private void addComponents()
	{
		//file=new JMenuItem("File",new ImageIcon("images/folders_16.gif"));
	//file=new JMenuItem("File",new KeyEvent.VK_F);
		this.setSize(500,500);
		this.setTitle(filename);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.textArea=new JTextArea();
		enableDragAndDrop();
		
		this.menuBar=new JMenuBar();
		this.file=new JMenu("File");
		this.edit=new JMenu("Edit");
		menuBar.add(file);
		menuBar.add(edit);
		
		this.newFile=new JMenuItem("New");
		KeyStroke keyStrokeToNew= KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK);
		newFile.setAccelerator(keyStrokeToNew);
		
		this.openFile=new JMenuItem("Open");
		KeyStroke keyStrokeToOpen= KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK);
		openFile.setAccelerator(keyStrokeToOpen);
		
		this.saveFile=new JMenuItem("Save");
		KeyStroke keyStrokeToSave= KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK);
		saveFile.setAccelerator(keyStrokeToSave);
		
		this.saveAsFile=new JMenuItem("Save As");
		this.exit=new JMenuItem("Exit");
		KeyStroke keyStrokeToExit= KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.CTRL_DOWN_MASK);
		exit.setAccelerator(keyStrokeToExit);
		
		file.add(newFile);
		file.add(openFile);
		file.add(saveFile);
		file.add(saveAsFile);
		file.addSeparator();
		file.add(exit);
		
		this.copy=new JMenuItem("Copy");
		KeyStroke keyStrokeToCopy= KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK);
		copy.setAccelerator(keyStrokeToCopy);
		
		this.cut=new JMenuItem("Cut");
		KeyStroke keyStrokeToCut= KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK);
		cut.setAccelerator(keyStrokeToCut);
		
		this.paste=new JMenuItem("Paste");
		KeyStroke keyStrokeToPaste= KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK);
		paste.setAccelerator(keyStrokeToPaste);
		
		this.print=new JMenuItem("Print");
		KeyStroke keyStrokeToPrint= KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK);
		print.setAccelerator(keyStrokeToPrint);
		
		this.search=new JMenuItem("Search");
		
		this.back=new JMenuItem("Back");
		KeyStroke keyStrokeToBack= KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK);
		back.setAccelerator(keyStrokeToBack);
		
		edit.add(copy);
		edit.add(cut);
		edit.add(paste);
		edit.addSeparator();
		edit.add(print);
		edit.add(search);
		edit.add(back);
		this.setJMenuBar(menuBar);
		
		panel=new JPanel();panel.setLayout(new BorderLayout());
		this.add(panel);		
		panel.add(textArea, BorderLayout.CENTER);
		
		componentsListener();
				
		this.setVisible(true);
	}
	private void componentsListener()
	{
		textArea.addKeyListener(new TextChanged(this));
		
		newFile.addActionListener(new NewFileListener(this));		
		openFile.addActionListener(new OpenListener(this));		
		saveFile.addActionListener(new SaveListener(this));		
		saveAsFile.addActionListener(new SaveAsListener(this));		
		exit.addActionListener(new ExitListener(this));
		
		copy.addActionListener(new CopyListener(this));
		cut.addActionListener(new CutListener(this));
		paste.addActionListener(new PasteListener(this));
		print.addActionListener(new PrintListener(this));
		search.addActionListener(new SearchListener(this));
		back.addActionListener(new BackListener(this));
	}
	
	private void toolsSetUp()
	{	
		tools=new JToolBar();
		//tools.setBackground(Color.GRAY);
		JButton btnew=new JButton(new ImageIcon("images/new.png"));
		btnew.addActionListener(new NewFileListener(this));
		JButton btopen=new JButton(new ImageIcon("images/open.png"));
		btopen.addActionListener(new OpenListener(this));
		JButton btsave=new JButton(new ImageIcon("images/save.png"));
		btsave.addActionListener(new SaveListener(this));
		JButton btprint=new JButton(new ImageIcon("images/print.png"));
		btprint.addActionListener(new PrintListener(this));
		JButton btcut=new JButton(new ImageIcon("images/cut.png"));
		btcut.addActionListener(new CutListener(this));
		JButton btcopy=new JButton(new ImageIcon("images/copy.png"));
		btcopy.addActionListener(new CopyListener(this));
		JButton btpaste=new JButton(new ImageIcon("images/paste.png"));
		btpaste.addActionListener(new PasteListener(this));
		JButton btfind=new JButton(new ImageIcon("images/find.png"));
		btfind.addActionListener(new SearchListener(this));
		
		tools.add(btnew);
		tools.add(btopen);
		tools.add(btsave);
		tools.add(btprint);
		tools.add(btcut);
		tools.add(btcopy);
		tools.add(btpaste);
		tools.add(btfind);
		
		panel.add(tools,BorderLayout.NORTH);

	}
	public String contentText()
	{
		return textArea.getText();
	}
	public String fileName()
	{
		JFileChooser chooser=new JFileChooser();
		int returnVal=chooser.showSaveDialog(this);
		File file=chooser.getSelectedFile();
		filename=file.toString();
		return filename;
	}
	public String saveFilename()
	{			
		filename=getName().substring(1);
		return filename;
	}
	public String fileNameOpen()
	{
		JFileChooser chooser=new JFileChooser();
		int returnVal=chooser.showOpenDialog(this);		
		File file=chooser.getSelectedFile();			
		filename=file.toString();
		return filename;
		
	}
	public void open(String text)
	{			
		textArea.append(text);
	}
	public String getName()
	{			
		return this.getTitle();
	}
	public void setName(String fn)
	{	
		this.setTitle(fn);
	}
	public void newFile(String text)
	{	
		textArea.setText(text);
		setName("Notepad");
	}
	private boolean changed=false;
	public boolean isChanged()
	{
		if(this.getName().contains("*"))
			changed=true;
		else
			changed=false;
		return changed;
	}	
	private int answer;
	public void failMsg()
	{
		JOptionPane j=new JOptionPane();
		answer=j.showConfirmDialog(null, "Do you want to save?");
		//if(answer==JOptionPane.YES_OPTION)			
	}
	public int getAnswer()
	{
		return answer;
	}
	
	public void checkButton(int choose)
	{
		c.handleEvent(choose);
	}
	public String copyText()
	{
		String text=textArea.getSelectedText();
		return text;
	}
	public String cutText()
	{
		String text=textArea.getSelectedText();
		textArea.replaceSelection("");
		return text;
	}
	public void pasteText(String text)
	{
		textArea.setText(contentText()+text);
	}
	
	
	
	
	/*	 * printmetoden	 */
	private Boolean isPrint=false;
	public void print()
	{
		try
		{
			//isPrint=true;
			Boolean complete=textArea.print();
			
			if(complete)
			{
				JOptionPane.showMessageDialog(null, "done printing...");
			}
			else
				JOptionPane.showMessageDialog(null, "printing...");
			}catch(PrinterException s)
			{
				
			}
		//return isPrint;
	}
	public Boolean getPrint()
	{
		return isPrint;
	}
	
	/* * findMetod	 */
	public void find()
	{
		Highlighter hl=textArea.getHighlighter();
		 String myWord = JOptionPane.showInputDialog(this, "Word to find?");
			
			String text = textArea.getText();        
			int index = text.indexOf(myWord);
			while(index >= 0){
			    try {        
                   Highlighter hilit = new DefaultHighlighter();
                   Highlighter.HighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);                          
                   hl.addHighlight(index, index + myWord.length(),painter);
			        //hl.addHighlight(index, index + myWord.length(), DefaultHighlighter.DefaultPainter);
			        index = text.indexOf(myWord, index + myWord.length());            
			    } catch (BadLocationException ex) {
			        ex.printStackTrace();
			    }
			} 
	}
	/*
	 * Drag and drop
	 */
	private void enableDragAndDrop()
    {
        DropTarget target=new DropTarget(textArea,new DropTargetListener(){
            public void dragEnter(DropTargetDragEvent e)
            {
            	if(e.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
					e.acceptDrag(DnDConstants.ACTION_COPY);
				else
					e.rejectDrag();
				
            }            
            public void dragExit(DropTargetEvent e)
            {
            }            
            public void dragOver(DropTargetDragEvent e)
            {
            }            
            public void dropActionChanged(DropTargetDragEvent e)
            {            
            }            
            public void drop(DropTargetDropEvent e)
            {
                try
                {
                    // Accept the drop first, important!
                    e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
                    
                    // Get the files that are dropped as java.util.List
                    java.util.List list=(java.util.List) e.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    
                    // Now get the first file from the list,
                    File file=(File)list.get(0);
                    setTitle(list.get(0).toString());
                    textArea.read(new FileReader(file),null);
                    
                }catch(Exception ex){}
            }
        });
    }
	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		
	}
	
}
