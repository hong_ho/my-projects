package View;

import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

import ControllerPackage.NotepadController;

public class ViewConsole implements ViewInterface {
	
	private String content="hong";
	private NotepadController c;
	private Scanner in;
	private int val=8;
	private String filename;
	
	
	public ViewConsole(NotepadController c) {	
			this.c=c;
			//setUp();		// ej ropas h�r f�r att konstruktorn inte k�r f�rdigt	
		}
	public void setUp()
	{		
		in=new Scanner(System.in);
		
		try {
            while (val!=0) {
                System.out.println("*********Menu*******************");
                System.out.println("1. New File");
                System.out.println("2. Open File");
                System.out.println("3. Save");
                System.out.println("4. Save As");
                System.out.println("5. Copy Text");
                System.out.println("6. Cut Text");
                System.out.println("7. Paste Text");
                System.out.println("0. Quit");
                System.out.println("Choose from 0 to 7: ");
                val=in.nextInt();
                
                //val=Integer.parseInt(enter);
                c.handleEvent(val);
           }
        }
        catch (Exception e) {
            System.out.println("Unexpected events in View.runUI exiting");
            System.exit(1);
        }
		
	}
	public boolean isChanged()
	{
		return false;
	}	
	
	public void failMsg()
	{
				
	}
	public int getAnswer()
	{
		return 1;
	}
	public void newFile(String s)
	{	
		content=s;
		System.out.println(content);
	}
	public String contentText()
	{
		return content;
	}
	public String saveFilename()
	{
		
		return filename;
	}
	public String fileName()
	{
		//if(val==4)
		return	fileSaveAs();
		//return filename;
	}
	
	//C:\Users\Hong\javakursen\labbar\NotepadLabb1\filename.txt
	public String fileNameOpen()
	{
		System.out.println("Mata in adress f�r att �ppna: ");
		in=new Scanner(System.in);
		filename=in.nextLine();
		return filename;
		
	}
	public String fileSaveAs()
	{
		System.out.println("Mata in adress f�r att spara som: ");
		in=new Scanner(System.in);
		filename=in.nextLine();
		return filename;
	}
	public void open(String text)
	{			
		System.out.println(text);
		in=new Scanner(System.in);
		content=text + in.nextLine();
		//System.out.println(content);
	}
	public String getName()
	{			
		return "";
	}
	public void setName(String fn)
	{	
		
	}
	
	//private boolean changed=false;

	public void checkButton(int choose)
	{
		c.handleEvent(choose);
	}
	public String copyText()
	{
		return null;
	}
	public String cutText()
	{
		return null;
	}
	public void pasteText(String text)
	{
		
	}
	
	
	
}
