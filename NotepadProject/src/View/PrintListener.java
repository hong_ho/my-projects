package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PrintListener implements ActionListener {

	private ViewGui v;
	public PrintListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(8);
		
	}

}

