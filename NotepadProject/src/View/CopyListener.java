package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CopyListener implements ActionListener{

	private ViewGui v;
	public CopyListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(5);
		
	}

}
