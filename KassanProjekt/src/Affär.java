import javax.swing.DefaultListModel;


public class Aff�r
{
	private DefaultListModel<Person> list;
	private KassaK� k1, k2;

	public Aff�r()
	{
		super();
		list=new DefaultListModel<Person>();
		k1=new KassaK�();
		k2=new KassaK�();
		
	}	
	public void add(Person elements) 
	{		
		list.addElement(elements);		
	}
	public void flytta(Person person, int kas)
	{
		list.removeElement(person);
		if(kas==1)
			k1.add(person);
		else if(kas==2)
			k2.add(person);	
	}	
	public boolean isEmpty() {
		
		return list.isEmpty();
	}	
	public DefaultListModel<Person> getList()
	{
		return list;
	}
	public DefaultListModel<Person> getListK1()
	{
		return k1.getList();
	}
	public DefaultListModel<Person> getListK2()
	{
		return k2.getList();
	}
}
